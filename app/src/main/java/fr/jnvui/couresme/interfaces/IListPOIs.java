package fr.jnvui.couresme.interfaces;

import java.util.HashMap;

import fr.jnvui.couresme.data.POI;

/**
 * Created by jips on 10/17/16.
 */

public interface IListPOIs {

    HashMap<String,POI> getPOIs();

}
